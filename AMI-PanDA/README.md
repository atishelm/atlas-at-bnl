# AMI / PanDA

The purpose of this directory is to keep track of useful tips etc. when using [AMI](https://atlas-ami.cern.ch/) and [PanDA](https://bigpanda.cern.ch/).

## PanDA job logs

How to view job logs, e.g. to check error messages of failed jobs.

* Go to a given task ID, e.g.: https://bigpanda.cern.ch/task/37399823/
* Click "Show jobs" --> "All (including retries)"
* Scroll down, then click on one failed job number on the leftmost column
* Then there is a Logs tab on the left -> click on "Log files"
* A useful one is "log.Derivation"
