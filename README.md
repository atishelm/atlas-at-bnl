# ATLAS at BNL

The purpose of this GitLab project is to keep track of files / scripts / etc. used for ATLAS physics analysis at BNL. 

## VOMS proxy 

In case you need to renew your voms proxy:

https://ca.cern.ch/ca/Help/?kbid=024010
