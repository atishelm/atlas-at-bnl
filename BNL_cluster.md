# BNL cluster 

Example of how to login to the BNL cluster:

```bash
sudo ssh -i .ssh/id_rsa atishelma@ssh.sdcc.bnl.gov
rterm -i spar0103
```

The first password will be your machine's sudo password, second will be the password of your BNL email/account.

# Look for files on EOS:

```
export KRB5CCNAME=$HOME/krb5cc_`id -u`
kinit atishelm@CERN.CH
xrdfs root://eospublic.cern.ch/ ls /eos/fcc/ee/generation/DelphesEvents/winter2023/IDEA/
```

# Jupyter hub

https://jupyter.sdcc.bnl.gov/
