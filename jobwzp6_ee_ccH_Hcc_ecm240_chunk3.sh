#!/bin/bash
source ${LOCAL_DIR}/setup.sh
mkdir jobwzp6_ee_ccH_Hcc_ecm240_chunk3
cd jobwzp6_ee_ccH_Hcc_ecm240_chunk3
$LOCAL_DIR/bin/fccanalysis run /direct/usatlas+u/atishelma/FCC/BNL-Analyses/ZccH_stage1.py --batch --output ZccH_4jetsRequired/stage1/chunk3.root --files-list  /eos/experiment/fcc/ee/generation/DelphesEvents/winter2023/IDEA/wzp6_ee_ccH_Hcc_ecm240/events_048684495.root
cp ZccH_4jetsRequired/stage1/chunk3.root  /usatlas/u/atishelma/FCC/BNL-Analyses/FCCAnalyses/ZccH_4jetsRequired/stage1//wzp6_ee_ccH_Hcc_ecm240/chunk3.root
