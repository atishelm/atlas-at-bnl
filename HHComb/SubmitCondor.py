"""
python3 SubmitCondor.py 
"""

#!/usr/bin/python
import sys, getopt
import itertools
import argparse
import operator
import os

if __name__ == '__main__':

  parser =  argparse.ArgumentParser()
  parser.add_argument('--NominalOnly',action="store_true",help = "Only run on nominal tree")
  args = parser.parse_args()

  scriptName = "condor_job.txt"

  local = os.getcwd()
  if not os.path.isdir('error'): os.mkdir('error') 
  if not os.path.isdir('output'): os.mkdir('output') 
  if not os.path.isdir('log'): os.mkdir('log') 
   
  # Prepare condor jobs
  condor = '''executable              = run_script.sh
output                  = output/$(ClusterId).$(ProcId).out
error                   = error/$(ClusterId).$(ProcId).err
log                     = log/$(ClusterId).log
transfer_input_files    = run_script.sh
getenv                  = True

+JobFlavour             = "workday" 
queue arguments from arguments.txt
'''

  with open(scriptName, "w") as cnd_out:
     cnd_out.write(condor)

  script = '''#!/bin/sh -e

WORKING_DIREC=$1 # directory you're running from
dataset=$2

echo "WORKING_DIREC: ${WORKING_DIREC}"
echo "dataset: ${dataset}"

echo "$ cd ${WORKING_DIREC}"
cd ${WORKING_DIREC} # directory you're running from

command="root -b -q -l 'MakeNtuple.C(\\"${dataset}\\")'"

echo "$ ${command}"
eval $command # run the command, evaluating the strings as their environment variables

echo -e "DONE";

'''

  arguments = []

  datasets = [
    "user.lderamo.mc16d.PhPy8_HHML_3l0tau_1L_SM.MxAODDetailedNoSkim.e8273_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    "user.lderamo.mc16d.PhPy8_HH4b_SM.MxAODDetailedNoSkim.e8222_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",

    # "user.lderamo.mc16d.MGPy8_hhml_2l0tau_l1cvv1cv1.MxAODDetailedNoSkim.e8263_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.MGPy8_hhml_3l0tau_l1cvv1cv1.MxAODDetailedNoSkim.e8263_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.MGPy8_hhml_bbZZ2l2q_l1cvv1cv1.MxAODDetailedNoSkim.e8263_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.MGPy8_hhml_bbZZ4l_SM.MxAODDetailedNoSkim.e8263_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.MGPy8_hh_bbWW_1lep_vbf_SM.MxAODDetailedNoSkim.e8263_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.MGPy8_hh_bbWW_ll_vbf_SM.MxAODDetailedNoSkim.e8263_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.MGPy8_hh_bbZZ_ll_vbf_SM.MxAODDetailedNoSkim.e8263_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.MGPy8_hh_bbbb_vbf_SM.MxAODDetailedNoSkim.e8263_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.MGPy8_hh_bbtt_ll_vbf_SM.MxAODDetailedNoSkim.e8263_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.MGPy8_hh_bbtt_vbf_SM.MxAODDetailedNoSkim.e8263_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHbbtt2l_SM.MxAODDetailedNoSkim.e8222_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHbbttHadHad_SM.MxAODDetailedNoSkim.e8273_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHbbttLepHad_SM.MxAODDetailedNoSkim.e8273_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHbbWW1L_SM.MxAODDetailedNoSkim.e8222_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHbbZZ2l_SM.MxAODDetailedNoSkim.e8222_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHML_2l0tau_1L_SM.MxAODDetailedNoSkim.e8273_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHML_2l1tau_1L_SM.MxAODDetailedNoSkim.e8273_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHML_3l1tau_1L_SM.MxAODDetailedNoSkim.e8273_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHML_4l0tau_1L_SM.MxAODDetailedNoSkim.e8273_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHML_4l1tau_1L_SM.MxAODDetailedNoSkim.e8273_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHML_bbZZ2l2q_1L_SM.MxAODDetailedNoSkim.e8339_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root",
    # "user.lderamo.mc16d.PhPy8_HHML_bbZZ4l_1L_SM.MxAODDetailedNoSkim.e8339_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root"
  ]

  for dataset in datasets:
    arguments.append("{} {}".format(local, dataset))

  # Save arguments to text file to be input for condor jobs 
  with open("arguments.txt", "w") as args:
    args.write("\n".join(arguments))

  with open("run_script.sh", "w") as rs:
    rs.write(script) 

  submitCommand = "condor_submit {scriptName}".format(scriptName=scriptName)
  print("$ {submitCommand}".format(submitCommand=submitCommand))
  os.system(submitCommand)
  print("DONE")