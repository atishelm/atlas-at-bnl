# HHComb overlap studies

The purpose of this directory is to store scripts for HHComb overlap studies.

Setup to run `MakeNtuple.C` or `SubmitCondor.py`:

```bash
cd <workingDirectory>
setupATLAS
asetup AnalysisBase,24.2.28,here
root -b -q -l 'MakeNtuple.C('\"user.lderamo.mc16d.MGPy8_hh_bbbb_vbf_SM.MxAODDetailedNoSkim.e8263_s3126_r10201_p5037_h027yybb_fix_lderamo_1_MxAOD.root\"')'
python3 SubmitCondor.py
``` 

Files:

`FindFiles_setup.sh`: Setup to use rucio for finding datasets. \
`GetDatasets.py`: Python script to download rucio datasets. \
`MakeNtuple.C`: Main root macro used to produce overlap ntuples. \
`MakeTable.py`: Python script to create latex style overlap table. \
`SubmitCondor.py`: Script to run MakeNtuple.C over HTCondor. 
