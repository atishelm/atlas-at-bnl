import uproot
import os
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.table import Table

table = """

\\begin{table}[htbp]
    \\centering
    \\caption{Overlap table}
    \\label{tab:Overlap_table}
    \\resizebox{\\textwidth}{!}{
    \\begin{tabular}{|c||c|c|c|c|c|c|c|}
    \\hline
    DSID & 1500 & 1501 & 1502 & 1503 & 1504 & 1505 & 1506 \\\ \\hline
"""

skipFile = [
    "PhPy8_HHML_3l0tau_1L_SM.MxAODDetailedNoSkim.e8273_s3126_r10201_p503", 
    "PhPy8_HH4b_SM.MxAODDetailedNoSkim.e8222_s3126_r10201_p5037_h027yybb"
]

#inDir = "/eos/user/a/atishelm/ntuples/HHComb/overlapFiles/"
inDir = "overlapFiles"
files = ["%s/%s"%(inDir, f) for f in os.listdir(inDir) if os.path.isfile("%s/%s"%(inDir, f)) and f.endswith(".root")]
regions = [1500, 1501, 1502, 1503, 1504, 1505, 1506]

data = []
columns = ['DSID', 1500, 1501, 1502, 1503, 1504, 1505, 1506]

for file in files:
    print("file:",file)
    skip = 0
    for skipStr in skipFile:
        if(skipStr in file): skip = 1
    if(skip): continue

    try: 
        f = uproot.open(file)
    except: 
        print("Couldn't open %s"%(file))
        continue

    t = f["overlap"]

    dsid = t["dsid"].array()
    event_number = t["event_number"].array()
    region_vals = t["region"].array()
    m_hh = t["m_hh"].array()
    nEntries = t["nEntries"].array()

    if(len(dsid) == 0): continue
    else:
        table_row = ""
        this_dsid = dsid[0]
        all_info = []
        vals = []
        for region in regions:
            print("region:",region)
            checks = [val == region for val in region_vals]
            N_in_region = sum(checks)
            print("N_in_region:",N_in_region)
            pct_in_region = float(N_in_region) / float(nEntries[0])
            pct_in_region = '{:0.3e}'.format(pct_in_region)
            # pct_in_region /= 100.
            # if(pct_in_region < 0.00001): pct_in_region = '{:0.3e}'.format(pct_in_region)
            # else: pct_in_region = '{:0.9e}'.format(pct_in_region)
            #vals.append(N_in_region)
            vals.append(pct_in_region)
        table_row = "%s & %s & %s & %s & %s & %s & %s & %s \\\  \\hline"%(dsid[0], vals[0], vals[1], vals[2], vals[3], vals[4], vals[5], vals[6])
        # table_row = table_row.replace("&", "\% &")
        table += "%s\n"%(table_row)

        print("dsid:",dsid[0])
        print("vals:",vals)
        all_info.append(int(dsid[0]))
        for val_ in vals: all_info.append(val_)
        data.append(all_info)

table +=     "\\end{tabular}}\n"
table += "\\end{table}\n"

print(table)

df = pd.DataFrame(columns=columns, data = data)

# Fill the DataFrame row by row
# for row_data in data:
    # print("row_data:",row_data)
    #df = df.append(pd.Series(row_data, index=columns), ignore_index=True)
    # df = df.append(row_data)

# Print the resulting DataFrame
print(df)

#import seaborn as sns
#cm = sns.light_palette("green", as_cmap=True)

#df.style.background_gradient(cmap=cm)

import numpy as np 
from pandas import DataFrame
import seaborn as sns

# Column to exclude from coloring
column_to_exclude = 'DSID'
DSID_vals = df['DSID']
# Create a new DataFrame without the specified column
df_without_column = df.drop(columns=column_to_exclude, errors='ignore')

# Convert object columns to numeric (if possible)
df_numeric = df_without_column.apply(pd.to_numeric, errors='ignore')
#df_numeric = df

# Plotting using seaborn heatmap
#ax = sns.heatmap(df_numeric, annot=True, cmap='viridis', yticklabels=False)
ax = sns.heatmap(df_numeric, annot=True, cmap='viridis')
ax.set_yticklabels(DSID_vals)
plt.yticks(rotation=0) 
plt.show()
# Add the excluded column back to the original DataFrame
#excluded_column = df[column_to_exclude]

#print("excluded_column:",excluded_column)
# Overlay the excluded column on top of the original heatmap
#ax.add_patch(plt.Rectangle((len(df_numeric.columns), 0), 1, len(df_numeric), fill=False, edgecolor='none', linewidth=0))


#df_numeric = df.apply(pd.to_numeric, errors='ignore')

# Plotting using seaborn heatmap
#sns.heatmap(df_numeric, annot=True, cmap='viridis', vmin = 0, vmax = 1)
plt.show()
