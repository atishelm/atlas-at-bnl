#!/bin/bash
source ${LOCAL_DIR}/setup.sh
mkdir jobwzp6_ee_ccH_Hcc_ecm240_chunk0
cd jobwzp6_ee_ccH_Hcc_ecm240_chunk0
$LOCAL_DIR/bin/fccanalysis run /afs/cern.ch/work/a/atishelm/private/FCC/BNL-Analyses/ZccH_final.py --batch --output /eos/user/a/atishelm/ntuples/FCC/ZccH_4JetReco/final/wzp6_ee_ccH_Hcc_ecm240/chunk0.root --files-list  /eos/user/a/atishelm/ntuples/FCC/ZccH_4JetReco/stage1//wzp6_ee_ccH_Hcc_ecm240/chunk0.root
