# Private MC production

The purpose of this README is to provide instructions on how to produce private MC for testing purposes.

## mc16 STDM3 example

To find how to properly set up an Athena environment and produce DAOD_STDM3 from an mc16 AOD file, first look up a previously produced mc16 DAOD_STDM3 dataset on AMI:

https://atlas-ami.cern.ch/

Datasets -> Simple search -> mc16 -> search "%mc16_13TeV.%.deriv.DAOD_STDM3.%" -> For a dataset, click "Rucio" --> From Contained tids (task IDs) copy a TID e.g. 33322476

Go to https://bigpanda.cern.ch/

On top right search bar, click "Job by ID" to see the secret dropdown, and select "Task by ID". Enter your tid from AMI e.g. 33322476 and search.

From this page you should have the `AthDerivation` version used, and associated flags for your command. In this example, `AthDerivation-21.2.118.0` is used, and from the Job parameters / transPath, one can see the main executable is `Reco_tf.py`, and the required flag for this derivation is `--reductionConf STDM3`. One can try running this themselves on lxplus assuming they have an AOD to run over - if not, one can search for an AOD dataset and download a single file with `rucio download --nrandom 1 <datasetName>`.

```
cd workingArea
cp /eos/user/t/twamorka/pourAbe/AOD.37010428._000002.pool.root.1 . # you need to do this because otherwise the file won't be accessible from your centos7 container
setupATLAS -c centos7
asetup AthDerivation,21.2.118.0
Reco_tf.py --inputAODFile=AOD.37010428._000002.pool.root.1 --outputDAODFile=test.root --reductionConf STDM3 --maxEvents 100
```

## mc20 PHYSLITE example

Assuming you have an AOD and want to produce a PHYSLITE derivation with the latest Athena version:

```
asetup Athena,master,latest
Derivation_tf.py \ 
  --CA True \
  --inputAODFile ../SHbbyy/mc20_13TeV.801370.Py8EG_A14NNPDF23LO_XHS_X750_S400_HyySbb.recon.AOD.e8531_e8455_a907_r14859_tid37146407_00/AOD.37146407._000002.pool.root.1 \
  --outputDAODFile test_PHYSLITE.root \
  --formats PHYSLITE \
  --maxEvents 100
```
