"""
python3 RunCondor.py 
"""

#!/usr/bin/python
import sys, getopt
import itertools
import argparse
import operator
import os

if __name__ == '__main__':

  parser =  argparse.ArgumentParser()
  parser.add_argument('--NominalOnly',action="store_true",help = "Only run on nominal tree")
  args = parser.parse_args()

  scriptName = "condor_job.txt"

  local = os.getcwd()
  if not os.path.isdir('error'): os.mkdir('error') 
  if not os.path.isdir('output'): os.mkdir('output') 
  if not os.path.isdir('log'): os.mkdir('log') 
   
  # Prepare condor jobs
  condor = '''executable              = run_script.sh
output                  = output/$(ClusterId).$(ProcId).out
error                   = error/$(ClusterId).$(ProcId).err
log                     = log/$(ClusterId).log
transfer_input_files    = run_script.sh

+JobFlavour             = "microcentury" 
queue arguments from arguments.txt
'''

  with open(scriptName, "w") as cnd_out:
     cnd_out.write(condor)

  script = '''#!/bin/sh -e

WORKING_DIREC=$1 # directory you're running quickstats from
chhh_min=$2
chhh_max=$3
job_granularity=$4

# set path properly to include quickstats command, set pythonpath properly to contain quickstats dependencies
# for python I *think* /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/lib/python3.9/site-packages contains the pip installed packages quickstats uses
# need LD_LIBRARY_PATH correct for shared libraries, e.g. used by pandas
export PATH=${5}
export PYTHONPATH=${6}
export LD_LIBRARY_PATH=${7}

WSpath="../EFT_workspaces_newCoeffs/workspace/WS-bbyy-non-resonant-non-param.root"
cache_option=" --cache" # check cache for existing results (output directory)

echo "WORKING_DIREC: ${WORKING_DIREC}"
echo "chhh_min: ${chhh_min}"
echo "chhh_max: ${chhh_max}"
echo "job_granularity: ${job_granularity}"

echo "PATH: ${PATH}"
echo "PYTHONPATH: ${PYTHONPATH}"
echo "LD_LIBRARY_PATH: ${LD_LIBRARY_PATH}"

echo "$ cd ${WORKING_DIREC}"
cd ${WORKING_DIREC} # directory you're running quickstats from

command='quickstats limit_scan -i ${WSpath} --param_expr "chhh=${chhh_min}_${chhh_max}_${job_granularity},ctthh=-3_3_${job_granularity}" --poi mu_XS_HH --outdir chhh_ctthh_2D_limit_scan_condor ${cache_option} --outname limits.json --blind --skip_log --verbosity "ERROR"'

echo "$ ${command}"
eval $command # run the command, evaluating the strings as their environment variables

echo -e "DONE";

'''

  # paths required to be redefined on the HTCondor worker node
  # Assuming the current environment works
  path = os.environ['PATH']
  pythonpath = os.environ['PYTHONPATH']
  ld_library_path = os.environ['LD_LIBRARY_PATH']

  print("path:",path)
  print("pythonpath:",pythonpath)
  print("ld_library_path:",ld_library_path)

  arguments = []

  segment_granularity = 0.1 # difference between min and max for a given job 
  overall_min = -10. # lowest value for all jobs
  job_granularity = 0.1 # spacing between min and max points in a given job
  NJobs = 200 # -10 to 10 with steps of 0.1

  #chhh_mins = [-10. + i*segment_granularity for i in range(0,200)] # full 
  chhh_mins = [overall_min + i*segment_granularity for i in range(0,NJobs)]

  for chhh_min in chhh_mins:
    chhh_max = chhh_min + segment_granularity
    arguments.append("{} {} {} {} {} {} {}".format(local, chhh_min, chhh_max, job_granularity, path, pythonpath, ld_library_path)) 

  # Save arguments to text file to be input for condor jobs 
  with open("arguments.txt", "w") as args:
    args.write("\n".join(arguments))

  with open("run_script.sh", "w") as rs:
    rs.write(script) 

  submitCommand = "condor_submit {scriptName}".format(scriptName=scriptName)
  print("$ {submitCommand}".format(submitCommand=submitCommand))
  os.system(submitCommand)
  print("DONE")
