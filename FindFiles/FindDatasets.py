"""
13 February 2024
Abraham Tishelman-Charny

The purpose of this script is to survey available MC.
"""

import os 

DSIDs_dict = {
  "SH" : [800904, 800913, 800915, 801367, 801370],
  "Single_Higgs" : [343981, 346214, 345318, 345317, 345319, 345061, 346525, 345315, 346188, 346486],
  "HH" : [600021, 503004],
  "QCD" : [364352, 345868, 345869, 700508, 700509],
  "Systematics" : [450529, 450517, 450535, 450518, 450541, 450519, 346526, 346879, 600051, 508679]
}

process_types = ["SH", "Single_Higgs", "HH", "QCD", "Systematics"]
campaigns = ["mc20_13TeV", "mc23_13p6TeV"]

rtags_dict = {
  "mc20_13TeV" : ["r13167", "r13144", "r13145"], # mc20a (2015+2016), mc20d (2017), mc20e (2018) Full sim. AF3 might differ in rtags.
  "mc23_13p6TeV" : ["r14622", "r15224"] # mc23a (2022), mc23d (2023)
}

dataformat = "PHYSLITE"

for procType in process_types:
  print("**********************")
  print("Process type:",procType)
  print("**********************")
  DSIDs = DSIDs_dict[procType]
  for DSID in DSIDs:
    for campaign in campaigns:
      rtags = rtags_dict[campaign]
      for rtag in rtags:
        cmd = f"rucio ls {campaign}.*{DSID}*.*.*.*{dataformat}*.*{rtag}* --filter type=DATASET"
        print("$",cmd)
        os.system(cmd)
