export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/
echo "$ setupATLAS"
setupATLAS
echo "$ voms"
voms 
echo "$ lsetup rucio"
lsetup rucio
echo "$ rucio ls mc20_13TeV.600021.PhPy8EG_PDF4LHC15_HHbbyy_cHHH01d*DAOD*"
rucio ls mc20_13TeV.600021.PhPy8EG_PDF4LHC15_HHbbyy_cHHH01d*DAOD*
